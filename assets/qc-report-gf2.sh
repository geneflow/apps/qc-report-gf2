#!/bin/bash -l

# Generate a QC report from FaQCs pdfs wrapper script


###############################################################################
#### Helper Functions ####
###############################################################################

## ****************************************************************************
## Usage description should match command line arguments defined below
usage () {
    echo "Usage: $(basename "$0")"
    echo "  --input_folder => Input Folder"
    echo "  --reference => Reference Sequence"
    echo "  --output => Output File"
    echo "  --exec_method => Execution method (singularity, auto)"
    echo "  --exec_init => Execution initialization command(s)"
    echo "  --help => Display this help message"
}
## ****************************************************************************

# report error code for command
safeRunCommand() {
    cmd="$@"
    eval "$cmd; "'PIPESTAT=("${PIPESTATUS[@]}")'
    for i in ${!PIPESTAT[@]}; do
        if [ ${PIPESTAT[$i]} -ne 0 ]; then
            echo "Error when executing command #${i}: '${cmd}'"
            exit ${PIPESTAT[$i]}
        fi
    done
}

# print message and exit
fail() {
    msg="$@"
    echo "${msg}"
    usage
    exit 1
}

# always report exit code
reportExit() {
    rv=$?
    echo "Exit code: ${rv}"
    exit $rv
}

trap "reportExit" EXIT

# check if string contains another string
contains() {
    string="$1"
    substring="$2"

    if test "${string#*$substring}" != "$string"; then
        return 0    # $substring is not in $string
    else
        return 1    # $substring is in $string
    fi
}



###############################################################################
## SCRIPT_DIR: directory of current script, depends on execution
## environment, which may be detectable using environment variables
###############################################################################
if [ -z "${AGAVE_JOB_ID}" ]; then
    # not an agave job
    SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
else
    echo "Agave job detected"
    SCRIPT_DIR=$(pwd)
fi
## ****************************************************************************



###############################################################################
#### Parse Command-Line Arguments ####
###############################################################################

getopt --test > /dev/null
if [ $? -ne 4 ]; then
    echo "`getopt --test` failed in this environment."
    exit 1
fi

## ****************************************************************************
## Command line options should match usage description
OPTIONS=
LONGOPTIONS=help,exec_method:,exec_init:,input_folder:,reference:,output:,
## ****************************************************************************

# -temporarily store output to be able to check for errors
# -e.g. use "--options" parameter by name to activate quoting/enhanced mode
# -pass arguments only via   -- "$@"   to separate them correctly
PARSED=$(\
    getopt --options=$OPTIONS --longoptions=$LONGOPTIONS --name "$0" -- "$@"\
)
if [ $? -ne 0 ]; then
    # e.g. $? == 1
    #  then getopt has complained about wrong arguments to stdout
    usage
    exit 2
fi

# read getopt's output this way to handle the quoting right:
eval set -- "$PARSED"

## ****************************************************************************
## Set any defaults for command line options
EXEC_METHOD="auto"
EXEC_INIT=":"
## ****************************************************************************

## ****************************************************************************
## Handle each command line option. Lower-case variables, e.g., ${file}, only
## exist if they are set as environment variables before script execution.
## Environment variables are used by Agave. If the environment variable is not
## set, the Upper-case variable, e.g., ${FILE}, is assigned from the command
## line parameter.
while true; do
    case "$1" in
        --help)
            usage
            exit 0
            ;;
        --input_folder)
            if [ -z "${input_folder}" ]; then
                INPUT_FOLDER=$2
            else
                INPUT_FOLDER="${input_folder}"
            fi
            shift 2
            ;;
        --reference)
            if [ -z "${reference}" ]; then
                REFERENCE=$2
            else
                REFERENCE="${reference}"
            fi
            shift 2
            ;;
        --output)
            if [ -z "${output}" ]; then
                OUTPUT=$2
            else
                OUTPUT="${output}"
            fi
            shift 2
            ;;
        --exec_method)
            if [ -z "${exec_method}" ]; then
                EXEC_METHOD=$2
            else
                EXEC_METHOD="${exec_method}"
            fi
            shift 2
            ;;
        --exec_init)
            if [ -z "${exec_init}" ]; then
                EXEC_INIT=$2
            else
                EXEC_INIT="${exec_init}"
            fi
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Invalid option"
            usage
            exit 3
            ;;
    esac
done
## ****************************************************************************

## ****************************************************************************
## Log any variables passed as inputs
echo "Input_folder: ${INPUT_FOLDER}"
echo "Reference: ${REFERENCE}"
echo "Output: ${OUTPUT}"
echo "Execution Method: ${EXEC_METHOD}"
echo "Execution Initialization: ${EXEC_INIT}"
## ****************************************************************************



###############################################################################
#### Validate and Set Variables ####
###############################################################################

## ****************************************************************************
## Add app-specific logic for handling and parsing inputs and parameters

# INPUT_FOLDER input

if [ -z "${INPUT_FOLDER}" ]; then
    echo "Input Folder required"
    echo
    usage
    exit 1
fi
# make sure INPUT_FOLDER is staged
count=0
while [ ! -d "${INPUT_FOLDER}" ]
do
    echo "${INPUT_FOLDER} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -d "${INPUT_FOLDER}" ]; then
    echo "Input Folder not found: ${INPUT_FOLDER}"
    exit 1
fi
INPUT_FOLDER_FULL=$(readlink -f "${INPUT_FOLDER}")
INPUT_FOLDER_DIR=$(dirname "${INPUT_FOLDER_FULL}")
INPUT_FOLDER_BASE=$(basename "${INPUT_FOLDER_FULL}")


# REFERENCE input

if [ -z "${REFERENCE}" ]; then
    echo "Reference Sequence required"
    echo
    usage
    exit 1
fi
# make sure REFERENCE is staged
count=0
while [ ! -f "${REFERENCE}" ]
do
    echo "${REFERENCE} not staged, waiting..."
    sleep 1
    count=$((count+1))
    if [ $count == 10 ]; then break; fi
done
if [ ! -f "${REFERENCE}" ]; then
    echo "Reference Sequence not found: ${REFERENCE}"
    exit 1
fi
REFERENCE_FULL=$(readlink -f "${REFERENCE}")
REFERENCE_DIR=$(dirname "${REFERENCE_FULL}")
REFERENCE_BASE=$(basename "${REFERENCE_FULL}")



# OUTPUT parameter
if [ -n "${OUTPUT}" ]; then
    :
    OUTPUT_FULL=$(readlink -f "${OUTPUT}")
    OUTPUT_DIR=$(dirname "${OUTPUT_FULL}")
    OUTPUT_BASE=$(basename "${OUTPUT_FULL}")
    LOG_FULL="${OUTPUT_DIR}/_log"
    TMP_FULL="${OUTPUT_DIR}/_tmp"
else
    :
    echo "Output File required"
    echo
    usage
    exit 1
fi

## ****************************************************************************

## EXEC_METHOD: execution method
## Suggested possible options:
##   auto: automatically determine execution method
##   singularity: singularity image packaged with the app
##   docker: docker containers from docker-hub
##   environment: binaries available in environment path

## ****************************************************************************
## List supported execution methods for this app (space delimited)
exec_methods="singularity auto"
## ****************************************************************************

## ****************************************************************************
# make sure the specified execution method is included in list
if ! contains " ${exec_methods} " " ${EXEC_METHOD} "; then
    echo "Invalid execution method: ${EXEC_METHOD}"
    echo
    usage
    exit 1
fi
## ****************************************************************************



###############################################################################
#### App Execution Initialization ####
###############################################################################

## ****************************************************************************
## Execute any "init" commands passed to the GeneFlow CLI
CMD="${EXEC_INIT}"
echo "CMD=${CMD}"
safeRunCommand "${CMD}"
## ****************************************************************************



###############################################################################
#### Auto-Detect Execution Method ####
###############################################################################

# assign to new variable in order to auto-detect after Agave
# substitution of EXEC_METHOD
AUTO_EXEC=${EXEC_METHOD}
## ****************************************************************************
## Add app-specific paths to detect the execution method.
if [ "${EXEC_METHOD}" = "auto" ]; then
    # detect execution method
    if command -v singularity >/dev/null 2>&1; then
        AUTO_EXEC=singularity
    else
        echo "Valid execution method not detected"
        echo
        usage
        exit 1
    fi
    echo "Detected Execution Method: ${AUTO_EXEC}"
fi
## ****************************************************************************



###############################################################################
#### App Execution Preparation, Common to all Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to prepare environment for execution
MNT=""; ARG=""; CMD0="mkdir -p ${TMP_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
## ****************************************************************************



###############################################################################
#### App Execution, Specific to each Exec Method ####
###############################################################################

## ****************************************************************************
## Add logic to execute app
## There should be one case statement for each item in $exec_methods
case "${AUTO_EXEC}" in
    singularity)
        MNT=""; ARG=""; CMD0="REFERENCE_LEN=$(awk '!/^>/ {len+=length($0)} END {print len}' < ${REFERENCE_FULL}) ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        MNT=""; ARG=""; CMD0="printf \"Sample Name\\t# Reads Before Trimming\\tGC Before Trimming\\tAverage Phred Before Trimming\\tCoverage Before Trimming\\t# Reads After Trimming\\t# Paired Reads After Trimming\\t# Unpaired Reads After Trimming\\tGC After Trimming\\tAverage Phred After Trimming\\tCoverage After Trimming\\n\" > ${OUTPUT_FULL}; for i in \$(find \"\${INPUT_FOLDER_FULL}\" | grep 'pdf$'); do
  BASENAME=\"\$(basename \$i)\";
  singularity -s exec \"${SCRIPT_DIR}/pdftotext.sif\" pdftotext -raw - - < \"\$i\" > \"${TMP_FULL}/\${BASENAME}.txt\" || continue;
  printf \"\${BASENAME}\" | awk -F'_qc_report' '{printf \$1\"\\t\"}';
  cat \"${TMP_FULL}/\${BASENAME}.txt\" | grep 'Reads #:' | sed -z 's/\\n/|/g' | sed 's/Reads #: //g' | sed 's/Paired //g' | sed 's/Unpaired //g' | awk -F'|' '{printf \$1\"\\t\"}';
  cat \"${TMP_FULL}/\${BASENAME}.txt\" | grep 'GC' | grep -v 'Reads' | sed -z 's/\\n/|/g' | sed 's/GC //g' | sed 's/ ± /|/g' | awk -F'|' '{printf \$1\"\\t\"}';
  cat \"${TMP_FULL}/\${BASENAME}.txt\" | grep 'Average' | grep -v 'Reads' | sed -z 's/\\n/|/g' | sed 's/Average: //g' | awk -F'|' '{printf \$1\"\\t\"}';
  cat \"${TMP_FULL}/\${BASENAME}.txt\" | grep 'Total bases:' | sed -z 's/\\n/|/g' | sed 's/Total bases: //g' | awk -F'|' '{x=\$1/${REFERENCE_LEN}} END {printf x\"\\t\"}';
  cat \"${TMP_FULL}/\${BASENAME}.txt\" | grep 'Reads #:' | sed -z 's/\\n/|/g' | sed 's/Reads #: //g' | sed 's/Paired //g' | sed 's/Unpaired //g' | awk -F'|' '{printf \$2\"\\t\"\$3\"\\t\"\$4\"\\t\"}';
  cat \"${TMP_FULL}/\${BASENAME}.txt\" | grep 'GC' | grep -v 'Reads' | sed -z 's/\\n/|/g' | sed 's/GC //g' | sed 's/ ± /|/g' | awk -F'|' '{printf \$3\"\\t\"}';
  cat \"${TMP_FULL}/\${BASENAME}.txt\" | grep 'Average' | grep -v 'Reads' | sed -z 's/\\n/|/g' | sed 's/Average: //g' | awk -F'|' '{printf \$2\"\\t\"}';
  cat \"${TMP_FULL}/\${BASENAME}.txt\" | grep 'Total bases:' | sed -z 's/\\n/|/g' | sed 's/Total bases: //g' | sed 's/ /|/g' | awk -F'|' '{x=\$2/${REFERENCE_LEN}} END {printf x\"\\n\"}';
done >> ${OUTPUT_FULL} ${ARG}"; CMD="${CMD0}"; echo "CMD=${CMD}"; safeRunCommand "${CMD}"; 
        ;;
esac
## ****************************************************************************



###############################################################################
#### Cleanup, Common to All Exec Methods ####
###############################################################################

## ****************************************************************************
## Add logic to cleanup execution artifacts, if necessary
## ****************************************************************************

